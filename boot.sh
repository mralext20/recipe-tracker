#!/usr/bin/env bash
python setup.py

exec gunicorn -b :5000 --access-logfile - --error-logfile - app:app