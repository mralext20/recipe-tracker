import os

import requests
from flask import render_template, request, redirect, url_for, flash, send_file
from app import app, db, imgur
from app.forms import SignupForm, LoginForm, RecipeForm
from app.models import Recipe, Chef
from flask_login import login_user, logout_user, current_user, login_required


@app.route('/')
@app.route('/index')
def index():
    recipes = Recipe.query
    if request.path == "/":
        page = request.args.get("page", 1, int)
        recipes = recipes.paginate(page, app.config["PER_PAGE"], False)
        return render_template('home.html', recipes=recipes)
    else:
        recipes = recipes.order_by('title').all()
        letters = {}
        for i in recipes:
            try:
                letters[i.title[0].upper()] += [i]
            except KeyError:
                letters[i.title[0].upper()] = [i]

        return render_template('index.html', title="Index", recipies=letters)


@app.route('/submit', methods=["GET", "POST"])
@login_required
def submit():
    """submit a recipe"""
    form = RecipeForm()
    if form.validate_on_submit():
        recipe = Recipe(
                        title=form.title.data,
                        serving_size=form.serving_size.data,
                        ingredients=form.ingredients.data,
                        instructions=form.instructions.data,
                        author=current_user)
        try:
            photo = request.files['photo']
            photo.save('photo.png')
            image = imgur.upload_image('photo.png')
            os.remove('photo.png')
            recipe.image_deletehash = image.deletehash
            recipe.image_link = image.link
        except KeyError:
            pass
        db.session.add(recipe)
        db.session.commit()
        return redirect(f'/recipe/{recipe.id}')
    return render_template('submit.html', title="submit", form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect('/')
    form = LoginForm()
    if form.validate_on_submit():
        user = Chef.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')

        login_user(user, remember=form.remember_me.data)
        return redirect('/')
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = SignupForm()
    if form.validate_on_submit():
        user = Chef(username=form.username.data, displayname=form.displayname.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        login_user(user, remember=form.remember_me.data)
        return redirect('/')
    return render_template('register.html', title='Register', form=form)


@app.route('/recipe/<int:rid>')
def recipe_route(rid):
    target = Recipe.query.filter_by(id=rid).one_or_none()
    if target is None:
        flash('that recipe doesnt exist!')
        return redirect('/')
    return render_template('recipe.html', title=target.title, recipe=target)


@app.route('/chef/<int:uid>')
def chef(uid):
    target = Chef.query.filter_by(id=uid).one_or_none()
    if target is None:
        flash("that chef doesnt exist")
        return redirect('/')
    page = request.args.get("page", 1, int)
    recipes = target.recipe_pagination(page, app.config["PER_PAGE"])
    return render_template('chef.html', title=f"Chef {target.displayname}", chef=target, recipes=recipes)


@app.route('/delete/chef/<int:uid>', methods=["POST"])
def delete_chef(uid):
    target = Chef.query.filter_by(id=uid).one_or_none()
    if target is None:
        flash("that Chef doesnt exist?")
        return redirect('/')
    if current_user.admin or current_user == target:
        if current_user == target:
            logout_user()
        [db.session.delete(recipe) for recipe in target.recipies]
        db.session.delete(target)
        db.session.commit()
        flash("successfully deleted user")
    else:
        flash('you arent allowed to access this')
    return redirect('/')


@app.route('/delete/recipe/<int:rid>', methods=["POST"])
def delete_recipe(rid):
    target = Recipe.query.filter_by(id=rid).one_or_none()
    if target is None:
        flash("that recipe doesnt exist?")
        return redirect('/')
    if current_user.admin or target.author == current_user:
        db.session.delete(target)
        db.session.commit()
        flash("successfully deleted recipe")
    else:
        flash('you arent allowed to access this')
    return redirect('/')


@app.route('/delete/recipe/<int:rid>/photo', methods=["POST"])
def delete_recipe_photo(rid):
    target = Recipe.query.filter_by(id=rid).one_or_none()
    if target is None:
        flash("that recipe doesnt exist?")
        return redirect('/')
    if current_user.admin or target.author == current_user:
        #  get, and delete the image from imgur
        requests.post(f"https://imgur.com/delete/{target.image_deletehash}")
        # delete the link from the DB
        target.image_deletehash = None
        target.image_link = None
        db.session.add(target)
        db.session.commit()
        flash("successfully deleted recipe photo")
    else:
        flash('you arent allowed to access this')

    return redirect(request.referrer)


@app.route('/edit/<int:rid>', methods=["GET", "POST"])
def edit_recipe(rid):
    target = Recipe.query.filter_by(id=rid).one_or_none()

    if (current_user.admin or target.author == current_user) or target is None:
        form = RecipeForm()

        if form.validate_on_submit():
            target.title = form.title.data
            target.serving_size = form.serving_size.data
            target.instructions = form.instructions.data
            target.ingredients = form.ingredients.data
            try:
                photo = request.files['photo']
                photo.save('photo.png')
                image = imgur.upload_image('photo.png')
                os.remove('photo.png')
                target.image_link = image.link
                target.image_deletehash = image.deletehash
            except KeyError:
                pass
            db.session.add(target)
            db.session.commit()
            flash(f'successfully edited {target.title}')
            return redirect(f'/recipe/{rid}')

    else:
        flash('you do not have permission to edit this recipe')
        redirect('/')
    form = RecipeForm(title=target.title,
                      serving_size=target.serving_size,
                      ingredients=target.ingredients,
                      instructions=target.instructions)
    return render_template('submit.html', title=f'Editing {target.title}', form=form)


@app.route('/chef/<int:uid>/make_admin', methods=["POST"])
def make_admin(uid):
    target = Chef.query.filter_by(id=uid).one_or_none()
    if target is None:
        flash('That user does not exist!')
        return redirect('/')
    if current_user.admin:
        target.admin = True
        db.session.add(target)
        db.session.commit()
        flash(f'made {target.displayname} an Admin!')
    else:
        flash("you are not allowed to make people admins")
    return redirect(f'/chef/{uid}')


@app.route('/chef/<int:uid>/take_admin', methods=["POST"])
def take_admin(uid):
    target = Chef.query.filter_by(id=uid).one_or_none()
    if target is None:
        flash('That user does not exist!')
        return redirect('/')
    if current_user.admin:
        target.admin = False
        db.session.add(target)
        db.session.commit()
        flash(f'Took admin from {target.displayname}')
    else:
        flash("you are not allowed to take admin from people")
    return redirect(f'/chef/{uid}')
